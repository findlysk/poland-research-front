function buildTotalPages(resultsPerPage, totalResults) {
    if (!resultsPerPage) return 0;
    if (totalResults === 0) return 1;
    return Math.ceil(totalResults / resultsPerPage);
}

function buildTotalResults(hits) {
    return hits.documentList.numberOfHits;
}

function convertIdName(fieldName) {
    if(fieldName[0] === '_') {
        return fieldName.substr(1)
    } else {
        return fieldName
    }
}

function getById(aggregations, fieldName) {
    return aggregations.find(function(element) {
        return element.id === fieldName;
    })
}

function getValueFacet(aggregations, fieldName) {
    if (
        aggregations &&
        getById(aggregations, fieldName) &&
        getById(aggregations, fieldName).filters &&
        getById(aggregations, fieldName).filters.length > 0
    ) {
        return [
            {
                field: fieldName,
                type: "value",
                data: getById(aggregations, fieldName).filters.map(bucket => ({
                    // Boolean values and date values require using `key_as_string`
                    value: bucket.displayName,
                    count: bucket.count
                }))
            }
        ];
    }
}

function buildStateFacets(aggregations) {
    const author = getValueFacet(aggregations, "author");
    const genre = getValueFacet(
        aggregations,
        "genre"
    );

    const facets = {
        ...(author && { author }),
        ...(genre && { genre })
    };

    if (Object.keys(facets).length > 0) {
        return facets;
    }
}

function buildResults(hits) {
    const addEachKeyValueToObject = (acc, [key, value]) => ({
        ...acc,
        [key]: value
    });

    return hits.map(record => {
        return Object.entries(record)
            .map(([fieldName, fieldValue]) => [
                convertIdName(fieldName),
                {raw: fieldValue, snippet: fieldValue}
            ])
            .reduce(addEachKeyValueToObject, {});
    });
}


/*
  Converts an Elasticsearch response to new application state
  When implementing an onSearch Handler in Search UI, the handler needs to convert
  search results into a new application state that Search UI understands.
  For instance, Elasticsearch returns "hits" for search results. This maps to
  the "results" property in application state, which requires a specific format. So this
  file iterates through "hits" and reformats them to "results" that Search UI
  understands.
  We do similar things for facets and totals.
*/
export default function buildState(response) {
    const results = buildResults(response.documentList.documents);
    const totalResults = buildTotalResults(response);
    const totalPages = buildTotalPages(response.documentList.pagination.hitsPerPage, totalResults);
    const facets = buildStateFacets(response.facets);
    return {
        results,
        totalPages,
        totalResults,
        ...(facets && { facets })
    };
}