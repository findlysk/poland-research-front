import React from "react";
import {SearchProvider, Results, SearchBox, PagingInfo, ResultsPerPage, Paging, Facet, ErrorBoundary} from "@elastic/react-search-ui";
import {Layout} from "@elastic/react-search-ui-views";

import "@elastic/react-search-ui-views/lib/styles/styles.css";
import buildState from "./handlers/buildState";
import WithSearch from "@elastic/react-search-ui/es/WithSearchRenderProps";
import {SingleLinksFacet} from "@elastic/react-search-ui-views/es";

export default function App() {

    return (
        <SearchProvider
            config={{
                onSearch: async state => {
                    const {resultsPerPage} = state;
                    console.log(state)
                    let currentPage = state.current;
                    let queryI3 = `${process.env.REACT_APP_SEARCH_URI}=${state.searchTerm}&offset=${(currentPage - 1) * resultsPerPage}&hits=${resultsPerPage}`;
                    if (state.filters.length > 0) {
                        state.filters.forEach(item => {
                            queryI3 += `&${item.field}=${item.values[0]}`
                        });
                    }
                    const otherServiceResponse = await fetch(
                        queryI3
                    );
                    let data = await otherServiceResponse.json();
                    return buildState(data);
                },
                initialState: {
                    resultsPerPage: 10
                }
            }}
        >
            <WithSearch
                mapContextToProps={({wasSearched}) => ({wasSearched})}
            >
                {({wasSearched}) => (
                    <div className="App">
                        <ErrorBoundary>
                            <Layout
                                header={<SearchBox/>}
                                bodyContent={
                                    <Results
                                        titleField="title"
                                        urlField="url"
                                    />
                                }
                                sideContent={
                                    <div>
                                        <Facet
                                            field="genre"
                                            label="Gatunek"
                                            view={SingleLinksFacet}
                                        />
                                        <Facet
                                            field="author"
                                            label="Author"
                                            view={SingleLinksFacet}
                                        />
                                    </div>
                                }
                                bodyHeader={
                                    <React.Fragment>
                                        {wasSearched && <PagingInfo/>}
                                        {wasSearched && <ResultsPerPage options={[10, 20, 40]}/>}
                                    </React.Fragment>
                                }
                                bodyFooter={<Paging/>}
                            />
                        </ErrorBoundary>
                    </div>
                )}
            </WithSearch>
        </SearchProvider>
    );
}